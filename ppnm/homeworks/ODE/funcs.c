#include <math.h>
#include <gsl/gsl_vector.h>

#include "funcs.h"

void harfunc(double var, gsl_vector* funcval, gsl_vector* funcder){
	gsl_vector_set(funcder, 0, gsl_vector_get(funcval, 1));
	gsl_vector_set(funcder, 1, - gsl_vector_get(funcval, 0));
}

void sir(double var, gsl_vector* funcval, gsl_vector* funcder){
	double population = 5808180;
	double contacttime = 2.5;
	double recoverytime = 25;

	double susc = gsl_vector_get(funcval, 0);
	double infect = gsl_vector_get(funcval, 1);

	double diffsusc = - (infect * susc)/(population * contacttime);
	double diffrem = infect / recoverytime;
	double diffinfect = - diffsusc - diffrem;

	gsl_vector_set(funcder, 0, diffsusc);
	gsl_vector_set(funcder, 1, diffinfect);
	gsl_vector_set(funcder, 2, diffrem);
}

void sir2(double var, gsl_vector* funcval, gsl_vector* funcder){
        double population = 5808180;
        double contacttime = 5;
        double recoverytime = 25;

        double susc = gsl_vector_get(funcval, 0);
        double infect = gsl_vector_get(funcval, 1);

        double diffsusc = - (infect * susc)/(population * contacttime);
        double diffrem = infect / recoverytime;
        double diffinfect = - diffsusc - diffrem;

        gsl_vector_set(funcder, 0, diffsusc);
        gsl_vector_set(funcder, 1, diffinfect);
        gsl_vector_set(funcder, 2, diffrem);
}

