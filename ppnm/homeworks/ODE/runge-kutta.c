#include <math.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>

#include "runge-kutta.h"
#include "funcs.h"

void rkstep12(
	void (*func)(double t,gsl_vector*y,gsl_vector*dydt), /* the f from dy/dt=f(t,y) */
	double t,              /* the current value of the variable */
	gsl_vector* yt,            /* the current value y(t) of the sought function */
	double step,              /* the step to be taken */
	gsl_vector* yh,             /* output: y(t+h) */
	gsl_vector* err             /* output: error estimate */
){
	int order = yt->size;
	gsl_vector* k_0 = gsl_vector_alloc(order);
	gsl_vector* k_12 = gsl_vector_alloc(order);
	gsl_vector* tempfunc = gsl_vector_alloc(order);

	func(t, yt, k_0);

	for(int i = 0; i < order; i++){
		double yt_i = gsl_vector_get(yt,i);
		double k_0_i = gsl_vector_get(k_0,i);
		double tempfunc_i = yt_i + k_0_i * step/2;
		gsl_vector_set(tempfunc, i, tempfunc_i);
	}
	func(t + 0.5 * step, tempfunc, k_12);

	for(int i = 0; i < order; i++){
		double yt_i = gsl_vector_get(yt,i);
                double k_12_i = gsl_vector_get(k_12,i);
                double tempfunc_i = yt_i + step * k_12_i;
                gsl_vector_set( yh, i, tempfunc_i);
	}

	for(int i = 0; i < order; i++){
                double k_0_i = gsl_vector_get(k_0,i);
                double k_12_i = gsl_vector_get(k_12,i);
                double temperr_i = (k_0_i - k_12_i) * step/2;
                gsl_vector_set(err, i, temperr_i);
        }
	gsl_vector_free(k_0);
	gsl_vector_free(k_12);
	gsl_vector_free(tempfunc);

}

void driver(
	void (*func)(double,gsl_vector*,gsl_vector*), /* right-hand-side of dy/dt=f(t,y) */
	double a,                     /* the start-point a */
	gsl_vector* ya,                     /* y(a) */
	double b,                     /* the end-point of the integration */
	gsl_vector* yb,                     /* y(b) to be calculated */
	double initstep,                     /* initial step-size */
	double acc,                   /* absolute accuracy goal */
	double eps,                    /* relative accuracy goal */
	FILE* myfile
){

	int order = ya -> size;
	double err;
	double normfunc;
	double tol;

	gsl_vector* yfuncval = gsl_vector_alloc(order);
	gsl_vector* errfuncval = gsl_vector_alloc(order);
	gsl_vector* yaCopy = gsl_vector_alloc(order);
	gsl_vector_memcpy(yaCopy, ya);

	double pos = a;
	while(pos<b){
		if(myfile != NULL){
			fprintf(myfile, "%.5g\t", pos);
			for(int i = 0; i < order; ++i){
				fprintf(myfile, "%.5g\t", gsl_vector_get(yaCopy, i));
			}
			if(func == harfunc){
				fprintf(myfile, "%.5g\n", sin(pos));
			}
			else{
				fprintf(myfile, "\n");
			}

		}
		double finstep;
		double step = initstep;

		if(pos + step > b){
			step = b-pos;
		}

		do{
			rkstep12(func, pos, yaCopy, step, yfuncval, errfuncval);

			err = gsl_blas_dnrm2(errfuncval);
			normfunc = gsl_blas_dnrm2(yfuncval);

			tol = (normfunc * eps + acc) * sqrt(step/(b - a));
			finstep = step;
			step *= pow(tol/err, 0.25) * 0.95;
		} while(err > tol);

		gsl_vector_memcpy(yaCopy, yfuncval);
		pos += finstep;
	}
	gsl_vector_memcpy(yb, yfuncval);

	gsl_vector_free(yfuncval);
	gsl_vector_free(errfuncval);
	gsl_vector_free(yaCopy);

}
