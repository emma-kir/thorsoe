#include <math.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>

#include "runge-kutta.h"
#include "funcs.h"

int main(int argc, char* argv[]){
	int harorder = 2;
	gsl_vector* finval = gsl_vector_alloc(harorder);
	gsl_vector* initval = gsl_vector_calloc(harorder);
	gsl_vector_set(initval,1,1);

	double left = 0.0;
	double right = 2*M_PI;
	double absacc = 1e-3;
	double relacc = 1e-3;
	double step = (right - left)/10;

	FILE* haroutput = fopen(argv[1],"w");
	driver(&harfunc, left, initval, right, finval, step, absacc, relacc, haroutput);
	fclose(haroutput);


	left = 0.0;
	right = 100;
//	step = (right-left)/10;
	int sirdim = 3;
	gsl_vector* sirfinval = gsl_vector_alloc(sirdim);
	gsl_vector* sirinitval = gsl_vector_calloc(sirdim);


	double populationSize   =   5808180;
	double wasInfected      =   237792;
	double recovered        =   226630;
	double isInfected       =   wasInfected - recovered;
	double dead             =   2441;
	double vaccinated       =   445566;
	double removed          =   dead + recovered + vaccinated;

	gsl_vector_set(sirinitval, 0, populationSize - isInfected - removed);
	gsl_vector_set(sirinitval, 1, isInfected);
	gsl_vector_set(sirinitval, 2, removed);

	FILE* siroutput = fopen(argv[2],"w");
	driver(&sir, left, sirinitval, right, sirfinval, step, absacc, relacc, siroutput);
	fclose(siroutput);

//	step = (left-right)/10;

	gsl_vector* sir2finval = gsl_vector_alloc(sirdim);
	gsl_vector* sir2initval = gsl_vector_calloc(sirdim);
	gsl_vector_set(sir2initval, 0, populationSize - isInfected - removed);
	gsl_vector_set(sir2initval, 1, isInfected);
	gsl_vector_set(sir2initval, 2, removed);

	FILE* sir2output = fopen(argv[3],"w");
	driver(&sir2, left, sir2initval, right, sir2finval, step, absacc, relacc, sir2output);
	fclose(sir2output);

}


