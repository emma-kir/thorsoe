#ifndef CLION_TESTING_FUNCTIONS_H
#define CLION_TESTING_FUNCTIONS_H

#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>

void harfunc(double var, gsl_vector* funcval, gsl_vector* funcder);
void sir(double var, gsl_vector* funcval, gsl_vector* funcder);
void sir2(double var, gsl_vector* funcval, gsl_vector* funcder);

#endif 
