#include <stdio.h>
#include <math.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <float.h>

#include "minimization.h"

void numeric_gradient (double func(gsl_vector*), gsl_vector* minimum, gsl_vector* gradient){
	long double stepSize =  2.22045e-10;
	double funcVal  =   func(minimum);
	int numOfDims   =   minimum -> size;

	for(int dimId = 0; dimId < numOfDims; ++dimId){
		double step;
		double minimum_i   =   gsl_vector_get(minimum, dimId);

		if (fabs(minimum_i) < stepSize) {
			step = stepSize;
		}
		else {
			step = fabs(minimum_i) * stepSize;
		}

		gsl_vector_set(minimum,  dimId,  minimum_i + step                 );
		gsl_vector_set(gradient, dimId,  (func(minimum) - funcVal) / step );
		gsl_vector_set(minimum,  dimId,  minimum_i - step                 );
	}
}


void quasinewton(double func(gsl_vector*), gsl_vector* min, double tol){
	double step = 2.22045e-10;
	int dim = min->size;

	int nostep = 0;
	int noreset = 0;
	int noscale = 0;

	gsl_matrix* invhessian = gsl_matrix_alloc(dim,dim);
	gsl_matrix_set_identity(invhessian);

	gsl_matrix* identity = gsl_matrix_alloc(dim,dim);
	gsl_matrix_set_identity(identity);

	gsl_vector* gradval = gsl_vector_alloc(dim);
	gsl_vector* newtonstep = gsl_vector_alloc(dim);
	gsl_vector* min_next = gsl_vector_alloc(dim);
	gsl_vector* gradval_next = gsl_vector_alloc(dim);
	gsl_vector* sol = gsl_vector_alloc(dim);
	gsl_vector* solchange = gsl_vector_alloc(dim);
	gsl_vector* broydenvec = gsl_vector_alloc(dim);


	numeric_gradient(func, min, gradval);
	double funcval = func(min);
	double funcval_next;

	while(nostep < 1e4){
		nostep++;

		gsl_blas_dgemv(CblasNoTrans, -1, invhessian, gradval, 0, newtonstep);
		if(gsl_blas_dnrm2(newtonstep) < step * gsl_blas_dnrm2(min)){
			printf("Quasi-newton method: |Dx| < stepSize * |x|\n");
			break;
		}
		if(gsl_blas_dnrm2(gradval) < tol){
			printf("Quasi-newton method: |grad| < acc\n");
			break;
		}

		double scale = 1;

		while(1){
			gsl_vector_memcpy(min_next, min);
			gsl_vector_add(min_next, newtonstep);

			funcval_next = func(min_next);

			double sTransGrad;
			gsl_blas_ddot(newtonstep, gradval, &sTransGrad);

			if(funcval_next < funcval + 0.01 * sTransGrad){
				noscale ++;
				break;
			}
			if(scale < step){
				noreset++;
				gsl_matrix_set_identity(invhessian);
				break;
			}
			scale *= 0.5;
			gsl_vector_scale(newtonstep, 0.5);
		}
		numeric_gradient(func, min_next, gradval_next);

		gsl_vector_memcpy(sol,gradval_next);
		gsl_blas_daxpy(-1, gradval, sol);
		gsl_vector_memcpy(solchange, newtonstep);
		gsl_blas_dgemv(CblasNoTrans, -1, invhessian, sol, 1, solchange);

		gsl_matrix* solchangesolchangetrans = gsl_matrix_calloc(dim,dim);
		gsl_blas_dsyr(CblasUpper, 1.0, solchange, solchangesolchangetrans);
		double solchangetranssol;
		gsl_blas_ddot(solchange, sol, &solchangetranssol);
		if(fabs(solchangetranssol) > 1e-12){
			gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0/solchangetranssol, solchangesolchangetrans, identity, 1.0,invhessian);
		}
		gsl_vector_memcpy(min,min_next);
		gsl_vector_memcpy(gradval, gradval_next);
		funcval = funcval_next;
	}
	gsl_matrix_free(invhessian);
	gsl_matrix_free(identity);
	gsl_vector_free(gradval);
	gsl_vector_free(newtonstep);
	gsl_vector_free(min_next);
	gsl_vector_free(gradval_next);
	gsl_vector_free(sol);
	gsl_vector_free(solchange);
	gsl_vector_free(broydenvec);

	printf("Quasi-newton method: \n  number of steps = %i,\n  number of scales = %i,\n  number of hessian matrix resets = %i\n  f(x) = %.1e\n\n", nostep, noscale, noreset, funcval);
}
