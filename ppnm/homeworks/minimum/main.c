#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <gsl/gsl_vector.h>

#include "minimization.h"

double testfunc (gsl_vector* vals){
	double x = gsl_vector_get(vals, 0);
	double y = gsl_vector_get(vals, 1);

	double a = 6;
	double b = 13;

	double funcval = 1 + (x-a)*(x-a) + (y-b)*(y-b);

	return funcval;
}

double rosenbrock(gsl_vector* vals) {
	double x = gsl_vector_get(vals, 0);
	double y = gsl_vector_get(vals, 1);

	double funcval = (1-x)*(1-x) + 100*(y-x*x)*(y-x*x);
	return funcval;
}

double himmelblau(gsl_vector* vals) {
	double x = gsl_vector_get(vals, 0);
	double y = gsl_vector_get(vals, 1);

	double funcval = (x*x+y-11)*(x*x + y - 11) + (x + y*y - 7)*(x + y*y - 7);
	return funcval;
}


int main(int argc, char* argv[]){
	double min_x = 6;
	double min_y = 13;
	double init_x = min_x - 3;
	double init_y = min_y - 2;

	int dim = 2;
	double tol = 1e-5;
	gsl_vector* min = gsl_vector_alloc(dim);
	gsl_vector_set(min, 0, init_x);
	gsl_vector_set(min, 1, init_y);

	quasinewton(testfunc, min, tol);

	printf("Finding the minimum of f(x,y) = 1 + x² + y²:\n");
	printf("Actual minimum: (x,y) = (%g,%g)\n", min_x, min_y);
	printf("Found minimum: (x,y) = (%g,%g)\n", gsl_vector_get(min,0),gsl_vector_get(min,1));

	min_x = 1;
	min_y = 1;
	init_x = 0;
	init_y = 0;

	gsl_vector_set(min, 0, init_x);
	gsl_vector_set(min, 1, init_y);

	quasinewton(rosenbrock, min, tol);

	printf("Finding the minimum of Rosenbrock's valley function:\n");
	printf("Actual minimum: is a parabola\n", min_x, min_y);
	printf("Found minimum: (x,y) = (%g,%g)\n", gsl_vector_get(min,0),gsl_vector_get(min,1));

	min_x = 3;
	min_y = 2;
	init_x = min_x - 0.2;
	init_y = min_y - 0.2;

	gsl_vector_set(min, 0, init_x);
	gsl_vector_set(min, 1, init_y);

	quasinewton(himmelblau, min, tol);

	printf("Finding the minimum of Himmelblau's function:\n");
	printf("Actual minimum: (x,y) = (%g,%g), (-2.80, 3.13), (-3.78, -3.28), and (3.58, -1.84)\n", min_x, min_y);
	printf("Found minimum: (x,y) = (%g,%g)\n", gsl_vector_get(min,0),gsl_vector_get(min,1));



}
