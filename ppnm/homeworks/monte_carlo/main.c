#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define RND ((double)rand()/RAND_MAX)

void random_numbers(int dim, const double* lower, const double* upper, double* numbers){
	for(int axis = 0; axis < dim; axis++){
		numbers[axis] = lower[axis] + RND * (upper[axis] - lower[axis]);
	}
}

void plain_monte_carlo(int dim, double* lower, double* upper, double function(double* numbers), int nop, double* res, double* err){
	double vol = 1;
	for(int axis = 0; axis < dim; axis++){
		vol *= upper[axis] - lower[axis];
	}

	double funcval;
	double vector[dim];
	double sum = 0;
	double sumsq = 0;

	for(int i = 0; i < nop; i++){
		random_numbers(dim, lower, upper, vector);
		funcval = function(vector);
		sum += funcval;
		sumsq += funcval * funcval;
	}
	double sumave = sum / nop;
	double var = sumsq / nop - sumave * sumave;
	*res = sumave * vol;
	*err = sqrt(var/nop) * vol;
}

double debug_func(double *x){
	return sqrt(x[0]);
}

double dmitri_func(double* x){
	return 1/ (pow(M_PI, 3)* (1-cos(x[0]) * cos(x[1]) * cos(x[2])));
}

int main(int argc, char* argv[]){
	int nop = (int) 1e6;

	double actvaltest = 1.8856180831641;

	const int dimdebug = 1;
	double* lowerdebug = calloc(dimdebug, sizeof(double));
	double* upperdebug = malloc(sizeof(double)*dimdebug);
	lowerdebug[0] = 0;
	upperdebug[0] = 2;
	double resdebug = 0;
	double errdebug = 0;

	plain_monte_carlo(dimdebug, lowerdebug, upperdebug, debug_func, nop, &resdebug, &errdebug);

	printf("∫_0^2 √(x):\n");
	printf("Exact value = %g\n", actvaltest);
	printf("Monte Carlo numerical estimate = %g\n", resdebug);
	printf("Difference = %g\n", fabs(resdebug-actvaltest));
	printf("Monte Carlo error estimate = %g\n", errdebug);

	double actvaldmitri = 1.3932039296856768591842462603255;

	const int dimdmitri = 3;
	double* lowerdmitri = calloc(dimdmitri, sizeof(double));
	double* upperdmitri = malloc(sizeof(double) * dimdmitri);
	for(int axis = 0; axis < dimdmitri; axis++){
		lowerdmitri[axis] = 0;
		upperdmitri[axis] = M_PI;
	}
	double resdmitri = 0;
	double errdmitri = 0;

	plain_monte_carlo(dimdmitri, lowerdmitri, upperdmitri, dmitri_func, nop, &resdmitri, &errdmitri);

	printf(" ∫_0^π dx/π ∫_0^π dz/π [1-cos(x)cos(y)cos(z)]^{-1} = Γ(1/4)4/(4π3):\n");
	printf("Exact value = %g\n", actvaldmitri);
	printf("Monte Carlo numerical estimate = %g\n", resdmitri);
	printf("Difference = %g\n", fabs(resdmitri-actvaldmitri));
	printf("Monte Carlo error estimate = %g\n", errdmitri);

}
