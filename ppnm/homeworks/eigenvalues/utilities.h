#ifndef HAVE_UTILITIES_H
#define HAVE_UTILITIES_H

#include <gsl/gsl_matrix.h>

void vector_print(char* string, gsl_vector* vector);
void print_matrix(int NoR, gsl_matrix* mTP, char* string );

#endif
