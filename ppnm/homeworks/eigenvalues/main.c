#include <stdio.h>
#include <stdlib.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <math.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_sort_vector.h>
#include <gsl/gsl_linalg.h>

#include "utilities.h"

void timesJ(gsl_matrix* A, int p, int q, double theta){
	double c=cos(theta),s=sin(theta);
	for(int i=0;i<A->size1;i++){
		double new_aip=c*gsl_matrix_get(A,i,p)-s*gsl_matrix_get(A,i,q);
		double new_aiq=s*gsl_matrix_get(A,i,p)+c*gsl_matrix_get(A,i,q);
		gsl_matrix_set(A,i,p,new_aip);
		gsl_matrix_set(A,i,q,new_aiq);
		}
}

void Jtimes(gsl_matrix* A, int p, int q, double theta){
	double c=cos(theta),s=sin(theta);
	for(int j=0;j<A->size2;j++){
		double new_apj= c*gsl_matrix_get(A,p,j)+s*gsl_matrix_get(A,q,j);
		double new_aqj=-s*gsl_matrix_get(A,p,j)+c*gsl_matrix_get(A,q,j);
		gsl_matrix_set(A,p,j,new_apj);
		gsl_matrix_set(A,q,j,new_aqj);
		}
}

void jacobi_diag(gsl_matrix* A, gsl_matrix* V){

	int changed;
	int n = (A->size1);

	do{
		changed=0;
		for(int p=0;p<n-1;p++)
		for(int q=p+1;q<n;q++){
			double apq=gsl_matrix_get(A,p,q);
			double app=gsl_matrix_get(A,p,p);
			double aqq=gsl_matrix_get(A,q,q);
			double theta=0.5*atan2(2*apq,aqq-app);
			double c=cos(theta),s=sin(theta);
			double new_app=c*c*app-2*s*c*apq+s*s*aqq;
			double new_aqq=s*s*app+2*s*c*apq+c*c*aqq;
			if(new_app!=app || new_aqq!=aqq) // do rotation
				{
				changed=1;
				timesJ(A,p,q, theta);
				Jtimes(A,p,q,-theta); // A←J^T*A*J 
				timesJ(V,p,q, theta); // V←V*J
				}
		}
	}while(changed!=0);

}

void main(){
	int n =5;

	gsl_matrix* A = gsl_matrix_alloc(n,n);
	gsl_matrix* ACopy = gsl_matrix_alloc(n,n);
	gsl_matrix* V = gsl_matrix_alloc(n,n);
	gsl_matrix* VCopy = gsl_matrix_alloc(n,n);
	gsl_matrix* D = gsl_matrix_alloc(n,n);
	gsl_matrix* DCopy = gsl_matrix_alloc(n,n);
	gsl_matrix* AtV = gsl_matrix_alloc(n,n);
	gsl_matrix* VtV = gsl_matrix_alloc(n,n);
	gsl_matrix* DtV = gsl_matrix_alloc(n,n);

	for(int i = 0; i < A -> size1; i++){
		for(int j = 0; j < A -> size1; j++){
			double num = (double) rand()/7000000;
			printf("%g\n",num);
			gsl_matrix_set(A,i,j,num);
			gsl_matrix_set(A,j,i,num);
		}
	}

	print_matrix(n,A,"A =");

	gsl_matrix_set_identity(V);
	gsl_matrix_memcpy(ACopy,A);

	jacobi_diag(A,V);

	gsl_matrix_memcpy(D,A);
	gsl_matrix_memcpy(DCopy,D);

	print_matrix(n,A,"D = ");

	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans, 1,ACopy, V,0,AtV);
	gsl_blas_dgemm(CblasTrans,CblasNoTrans, 1, V, AtV,0, D);
	print_matrix(n,D,"V^T*A*V = (should be equal to D)");

	gsl_blas_dgemm(CblasNoTrans, CblasTrans, 1, DCopy, V, 0,DtV);
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,1,V,DtV,0,ACopy);
	print_matrix(n,ACopy,"V*D*V^T = (should be equal to D)");

	gsl_matrix_memcpy(VCopy, V);
	gsl_blas_dgemm(CblasTrans,CblasNoTrans,1,VCopy,V,0,VtV);
	print_matrix(n,VtV,"V^T*V = (should be equal to I)");

}

