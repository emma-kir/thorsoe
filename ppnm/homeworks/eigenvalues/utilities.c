#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_vector.h>
#include <assert.h>

#include "utilities.h"

void print_matrix(int NoR, gsl_matrix* mTP, char* string){
        printf("\n%s\n", string);
        for(int row = 0; row < NoR; row++){
                gsl_vector_view mTP_row = gsl_matrix_row(mTP, row);
                gsl_vector* vec = &mTP_row.vector;

                for(int iter = 0; iter < vec -> size; iter++){
                        if(gsl_vector_get(vec,iter) > 1e-10){
                                printf("%10g\t",gsl_vector_get(vec,iter));
                        }
                        else {printf("%10g\t", 0.0);}
                }
        printf("\n");
        }
}

void vector_print(char* string, gsl_vector* vector){
        printf("%s\n", string);
        for (int iter = 0; iter < vector -> size; iter++){
                printf("%10g", gsl_vector_get(vector, iter));
        }
        printf("\n");
}


