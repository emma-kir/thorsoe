#include <math.h>
#include <assert.h>
#include <stdio.h>

#include "integration.h"

double adapt24 ( double func(double), double leftEndPt, double rightEndPt, double absAcc, double relAcc, double second_funcVal, double third_funcVal, int numOfRecursions, double* integrationError){
    double first_funcVal    =   func( leftEndPt + 1*( rightEndPt - leftEndPt )/6 );
    double fourth_funcVal   =   func( leftEndPt + 5*( rightEndPt - leftEndPt )/6 );

    double higherOrderEval  =   ( 2*first_funcVal + second_funcVal + third_funcVal + fourth_funcVal*2 ) * ( rightEndPt - leftEndPt ) /6;
    double lowerOrderEval   =   (   first_funcVal + second_funcVal + third_funcVal + fourth_funcVal   ) * ( rightEndPt - leftEndPt ) /4;

    double tolerance        =   absAcc + relAcc * fabs(higherOrderEval);
    double error            =   fabs( higherOrderEval - lowerOrderEval );

    if( error < tolerance ){
        *integrationError += error;
        return higherOrderEval;
    }
    else{
        double new_higherOrderEval_left  = adapt24( func,leftEndPt, (leftEndPt + rightEndPt) / 2, absAcc/ sqrt(2.), relAcc, first_funcVal, second_funcVal, numOfRecursions + 1, integrationError ) ;
        double new_higherOrderEval_right = adapt24( func, ( leftEndPt + rightEndPt ) / 2 , rightEndPt, absAcc / sqrt(2.), relAcc, third_funcVal, fourth_funcVal, numOfRecursions + 1, integrationError ) ;

        return new_higherOrderEval_left + new_higherOrderEval_right ;
    }
}

double adapt ( double func(double), double leftEndPt, double rightEndPt, double absAcc, double relAcc, double* integrationError ){
    double  second_funcVal      =   func( leftEndPt + 2*( rightEndPt - leftEndPt ) / 6 );
    double  third_funcVal       =   func( leftEndPt + 4*( rightEndPt - leftEndPt ) / 6 );

    int     numOfRecursions     =   0;

    return adapt24 ( func , leftEndPt , rightEndPt ,absAcc , relAcc , second_funcVal , third_funcVal , numOfRecursions, integrationError);
}


double integrate(double func(double), double a, double b, double absacc, double relacc, double* interr){
	if (isinf(-a)){
		if(isinf(b)){
			double transint (double t){
				return func(t / (1-t*t)) * (1+t*t)/pow(1-t*t,2);
			}
			return adapt(transint, -1, 0, absacc, relacc, interr);
		}
		else{
			double transint (double t){
				return func(b + 1 / (1 + t)) / pow(1-t, 2);
			}
			return adapt(transint, -1, 0, absacc, relacc, interr);
		}
	}
	else if (isinf(b)){
		double transint(double t){
			return func(a + t /(1 - t))/ pow(1-t, 2);
		}
		return adapt(transint, 0, 1, absacc, relacc, interr);
	}
	else{return adapt(func, a, b, absacc, relacc, interr);}
}
