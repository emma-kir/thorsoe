#include <math.h>
#include <stdio.h>
#include <gsl/gsl_integration.h>

#include "integration.h"


void print_testResults(char* string, double integralVal, double exactVal, double absAcc, double relAcc, double integrationError, int numOfCalls){
    printf("\n%s  : %g\n", string, integralVal);
//    printf("Error goal                       : %.25g\n", absAcc + fabs( exactVal ) * relAcc);
//    printf("Actual error                     : %.25g\n", fabs(integralVal - exactVal));
//    printf("Computed error estimate          : %.25g\n", integrationError);
//    printf("Function was called %i times...\n", numOfCalls);
}

void print_whichTest(char* string, double exactVal){
    printf("\n%s %g", string, exactVal);
    printf("\n---------------------------------");
}

int main(int argc, char* argv[]){
	double a = 0;
	double b = 1;
	double absacc = 1e-3;
	double relacc = 1e-3;

	int noc = 0;
	double interr = 0;

	double exactval = 2./3.;
	print_whichTest("∫_0^1 dx √(x) = 2/3 =", exactval);

	double firsttestfunc(double x){
		noc++;
		return sqrt(x);
	};

	double intval = integrate(firsttestfunc, a, b, absacc, relacc, &interr);
	print_testResults("Numerical integration gives", intval, exactval, absacc, relacc, interr, noc);

	double sectestfunc(double x){
		noc++;
		return 4* sqrt(1-x*x);
	};
	noc = 0;
	interr = 0;
	intval = integrate(sectestfunc, a, b, absacc, relacc, &interr);
	exactval = M_PI;
	print_whichTest("∫_0^1 dx 4√(1-x²) = π =", exactval);
	print_testResults("Numerical integration gives", intval, exactval, absacc, relacc, interr, noc);
}
