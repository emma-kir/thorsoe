#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_vector.h>
#include <assert.h>

#include "utilities.h"
#include "backsub.h"


void GS_decomp(gsl_matrix* A, gsl_matrix* R){
	int NoR =  A -> size1;
	int NoC =  A -> size2;
	assert(NoR >= NoC);

	for(int colu = 0; colu < NoC; colu++){
		gsl_vector* col = gsl_vector_alloc(NoR);
		*col = gsl_matrix_column(A, colu).vector;
		double colnorm = gsl_blas_dnrm2(col);
		gsl_matrix_set(R, colu, colu, colnorm);

		gsl_vector* oMC = gsl_vector_alloc(NoR);

		gsl_vector_memcpy(oMC, col);
		gsl_vector_scale(oMC, 1./colnorm);
		gsl_matrix_set_col(A, colu, oMC);

		for(int ncolu = colu+1; ncolu < NoC; ncolu++){
			gsl_vector* ncol = gsl_vector_alloc(NoR);
			*ncol = (gsl_matrix_column(A, ncolu)).vector;

			double tME;
			gsl_blas_ddot(oMC, ncol, &tME);
			gsl_matrix_set(R,colu,ncolu,tME);
			gsl_vector* oCS = gsl_vector_alloc(NoR);
			gsl_vector_memcpy(oCS,oMC);
			gsl_vector_scale(oCS, tME);
			gsl_vector_sub(ncol,oCS);

			gsl_matrix_set_col(A,ncolu,ncol);

			gsl_vector_free(ncol);
			gsl_vector_free(oCS);
		}
		gsl_vector_free(col);
		gsl_vector_free(oMC);

	}
}

void GS_solve(gsl_matrix* Q, gsl_matrix* R, gsl_vector* b, gsl_vector* xvec){
	gsl_blas_dgemv(CblasTrans, 1, Q, b, 0, xvec);
	backsub(R, xvec);
}

void GS_inv(gsl_matrix* Q, gsl_matrix* R, gsl_matrix* B){
	int dim = (R->size1);

	gsl_matrix* R_inv = gsl_matrix_alloc(dim,dim);
	gsl_vector* uvec = gsl_vector_alloc(dim);

	for(int i = 0; i < dim; i++){
		gsl_vector_set_basis(uvec,i);
		backsub(R,uvec);
		gsl_matrix_set_col(R_inv,i,uvec);
	}
	gsl_blas_dgemm(CblasNoTrans, CblasTrans, 1, R_inv, Q, 0, B);

	gsl_matrix_free(R_inv);
	gsl_vector_free(uvec);
}

int main(){

	int n = 5;
	int m = 4;
	
	double mrand = (double)RAND_MAX;

	gsl_matrix* A = gsl_matrix_alloc(n,m);
	gsl_matrix* Q = gsl_matrix_alloc(n,m);
	gsl_matrix* R = gsl_matrix_alloc(m,m);
	gsl_matrix* QtQ = gsl_matrix_alloc(m,m);
	gsl_matrix* QR_check = gsl_matrix_alloc(n,m);
	gsl_matrix* A_new = gsl_matrix_alloc(n,n);
	gsl_matrix* Q_new = gsl_matrix_alloc(n,n);
	gsl_matrix* R_new = gsl_matrix_alloc(n,n);
	gsl_vector* b = gsl_vector_alloc(n);
	gsl_vector* x = gsl_vector_alloc(n);
	gsl_vector* veccheck = gsl_vector_alloc(n);
	gsl_matrix* invmat = gsl_matrix_alloc(n,n);
	gsl_matrix* invcheck = gsl_matrix_alloc(n,n);

	for(int i = 0; i < n; i++){
		for(int j = 0; j < m; j++){
			double A_ij = rand()/mrand;
			gsl_matrix_set(A,i,j,A_ij);
		}
	}
	gsl_matrix_memcpy(Q,A);
	GS_decomp(A,R);

	gsl_blas_dgemm(CblasTrans,CblasNoTrans, 1, A, A, 0, QtQ);



	print_matrix(m,R,"R =");
	print_matrix(m,QtQ,"Q^T * Q =");


	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans, 1, A, R, 0, QR_check);

	print_matrix(n,QR_check,"Q*R = ");
	print_matrix(n, Q, "should be equal to:");

//-------------------------------------------------------------------------------------

	for(int i = 0; i < n; i++){
		for(int j = 0; j < n; j++){
			double A_ij = rand()/mrand;
			gsl_matrix_set(A_new,i,j,A_ij);
		}
	}
	for(int i = 0; i < n; i++){
		double b_i = rand()/mrand;
		gsl_vector_set(b,i,b_i);
	}

//	print_matrix(n,A_new, "square matrix");
	gsl_matrix_memcpy(Q_new,A_new);
	GS_decomp(Q_new,R_new);
//	print_matrix(n,Q_new,"Q_new");

//	gsl_blas_dgemv(CblasTrans, 1, Q_new, b, 0, x);
//	backsub(R_new,x);

	GS_solve(Q_new,R_new,b,x);

	gsl_blas_dgemv(CblasNoTrans, 1, A_new, x, 0, veccheck);
	vector_print("A*x =", veccheck);

	vector_print("should be equal to:",b);

	GS_inv(Q_new, R_new, invmat);
	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans, 1, A_new, invmat, 0, invcheck);
	print_matrix(n, invcheck, "A*A^{-1} = (Should be identity matrix)");

}

