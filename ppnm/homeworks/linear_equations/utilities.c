#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include "utilities.h"

double randnum(unsigned int *seed){
	double maxn = (double)RAND_MAX;
	double randn = (double)rand_r(seed);
	return randn/maxn;
}


void vector_print(char* string, gsl_vector* vec){
	printf("%s\n", string);
	for (int iter = 0; iter < vec -> size; iter++){
		printf("%10g", gsl_vector_get(vec, iter));
	}
	printf("\n");
}

void set_data(gsl_matrix* tmat, gsl_vector* tRvec, unsigned int *seed){
	for(int row = 0; row < tmat -> size1; row++){
		for(int colu = 0; colu < tmat -> size2; colu++){
			gsl_matrix_set(tmat, row, colu, randnum(seed));
		}
		gsl_vector_set(tRvec, row, randnum(seed));
	}
}

void print_matrix(int NoR, gsl_matrix* mTP, char* string){
	printf("\n%s\n", string);
	for(int row = 0; row < NoR; row++){
		gsl_vector_view mTP_row = gsl_matrix_row(mTP, row);
		gsl_vector* vector = &mTP_row.vector;

		for(int iter = 0; iter < vector -> size; iter++){
			if(gsl_vector_get(vector,iter) > 1e-10){
				printf("%10g\t",gsl_vector_get(vector,iter));
			}
			else {printf("%10g\t", 0.0);}
		}
	printf("\n");
	}
}
