#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>

#include "backsub.h"

void backsub(gsl_matrix* A, gsl_vector* Rvec){
	int NoR = (Rvec -> size);

	for (int row = NoR - 1; row >= 0; row--){
		double Rval = gsl_vector_get(Rvec, row);

		for(int vari = row + 1; vari < NoR; vari++){
			Rval -= gsl_matrix_get(A, row, vari)*gsl_vector_get(Rvec,vari);
		}
	gsl_vector_set(Rvec, row, Rval/gsl_matrix_get(A, row, row));
	}
}
