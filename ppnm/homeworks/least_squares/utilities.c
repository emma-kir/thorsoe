#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_vector.h>
#include <assert.h>

#include "utilities.h"
#include "backsub.h"


double funs(int i, double x){
	switch(i){
		case  0: return 1  ; break;
		case  1: return x  ; break;
		case  2: return x*x; break;
		default: return NAN;
		}
	}

void GS_decomp(gsl_matrix* A, gsl_matrix* R){
        int NoR = A -> size1;
        int NoC = A -> size2;
        assert(NoR >= NoC);

        for(int colu = 0; colu < NoC; colu++){
                gsl_vector* col = gsl_vector_alloc(NoR);
                *col = gsl_matrix_column(A, colu).vector;
                double colnorm = gsl_blas_dnrm2(col);
                gsl_matrix_set(R, colu, colu, colnorm);

                gsl_vector* oMC = gsl_vector_alloc(NoR);

                gsl_vector_memcpy(oMC, col);
                gsl_vector_scale(oMC, 1./colnorm);
                gsl_matrix_set_col(A, colu, oMC);

                for(int ncolu = colu+1; ncolu < NoC; ncolu++){
                        gsl_vector* ncol = gsl_vector_alloc(NoR);
                        *ncol = (gsl_matrix_column(A, ncolu)).vector;

                        double tME;
                        gsl_blas_ddot(oMC, ncol, &tME);
                        gsl_matrix_set(R,colu,ncolu,tME);
                        gsl_vector* oCS = gsl_vector_alloc(NoR);
                        gsl_vector_memcpy(oCS,oMC);
                        gsl_vector_scale(oCS, tME);
                        gsl_vector_sub(ncol,oCS);

                        gsl_matrix_set_col(A,ncolu,ncol);

                        gsl_vector_free(ncol);
                        gsl_vector_free(oCS);
                }
                gsl_vector_free(col);                gsl_vector_free(oMC);

        }
}



void GS_solve(gsl_matrix* Q, gsl_matrix* R, gsl_vector* b, gsl_vector* x){

    //Solves the equation  QRx=b by applying Q^T to the vector b and then performin>

    gsl_blas_dgemv(CblasTrans, 1., Q,b,0.,x); //Computes the matrix vector product >
    backsub(R,x); //Compute back substitution of R and x and returns answer in x
}


void GS_inv(gsl_matrix* Q, gsl_matrix* R, gsl_matrix* B){
        int dim = (R->size1);

        gsl_matrix* R_inv = gsl_matrix_alloc(dim,dim);
        gsl_vector* uvec = gsl_vector_alloc(dim);

        for(int i = 0; i < dim; i++){
                gsl_vector_set_basis(uvec,i);
                backsub(R,uvec);
                gsl_matrix_set_col(R_inv,i,uvec);
        }
        gsl_blas_dgemm(CblasNoTrans, CblasTrans, 1, R_inv, Q, 0, B);

        gsl_matrix_free(R_inv);
        gsl_vector_free(uvec);
}


double randnum(unsigned int *seed){
        double maxn = (double) RAND_MAX;
        double randn = (double) rand_r(seed);
        return randn/maxn;
}


void vector_print(char* string, gsl_vector* vector){
        printf("%s\n", string);
        for (int iter = 0; iter < vector -> size; iter++){
                printf("%10g", gsl_vector_get(vector, iter));
        }
        printf("\n");
}

void set_data(gsl_matrix* tmat, gsl_vector* tRvec, unsigned int *seed){
        for(int row = 0; row < tmat -> size1; row++){
                for(int colu = 0; colu < tmat -> size2; colu++){
                        gsl_matrix_set(tmat, row, colu, randnum(seed));
                }
                gsl_vector_set(tRvec, row, randnum(seed));
        }
}

void print_matrix(int NoR, gsl_matrix* mTP, char* string){
        printf("\n%s\n", string);
        for(int row = 0; row < NoR; row++){
                gsl_vector_view mTP_row = gsl_matrix_row(mTP, row);
                gsl_vector* vec = &mTP_row.vector;

                for(int iter = 0; iter < vec -> size; iter++){
                        if(gsl_vector_get(vec,iter) > 1e-10){
                                printf("%10g\t",gsl_vector_get(vec,iter));
                        }
                        else {printf("%10g\t", 0.0);}
                }
        printf("\n");
        }
}

