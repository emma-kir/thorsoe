#ifndef HAVE_BACKSUB_H
#define HAVE_BACKSUB_H

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>

void backsub(gsl_matrix* A, gsl_vector* R);

#endif
