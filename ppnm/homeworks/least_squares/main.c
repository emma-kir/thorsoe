#include <stdio.h>
#include <stdlib.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <math.h>

#include "utilities.h"
#include "backsub.h"

typedef double (*f)(double x);

double f_0(double x){
	return 1;
}
double f_1(double x){
	return x;
}
void ls_fits(gsl_matrix* data, f functions[], int nfuncs, gsl_vector* c,gsl_matrix* covmat){
	gsl_matrix* A = gsl_matrix_alloc(data -> size1,nfuncs);
	gsl_matrix* R = gsl_matrix_alloc(data -> size1,nfuncs);
	gsl_matrix* cov = gsl_matrix_alloc(covmat -> size1, covmat -> size2);
	gsl_matrix* covcop = gsl_matrix_alloc(covmat -> size1, covmat ->size2);
	gsl_vector_view y = gsl_matrix_column(data,1);

	for(int i = 0; i < (A->size1); i++){
		double x_i = gsl_matrix_get(data,i,0);
		for(int j = 0; j < (A->size2); j++){
			double A_ij = functions[j](x_i);
			gsl_matrix_set(A,i,j,A_ij);
		}
	}

	GS_decomp(A,R);
	GS_solve(A,R,(&y.vector),c);

	gsl_blas_dgemm(CblasTrans,CblasNoTrans, 1, R, R, 0, covcop);
	GS_decomp(covcop,cov);
	GS_inv(covcop,cov,covmat);

	gsl_matrix_free(A);
	gsl_matrix_free(R);
	gsl_matrix_free(covcop);
	gsl_matrix_free(cov);
}

int main(){
	int ndata = 9;
	int nfuncs = 2;
	double x[] = {1.,2.,3.,4.,6.,9.,10.,13.,15.};
	double y[] = {117.,100.,88.,72.,53.,29.5,25.2,15.2,11.1};

	gsl_matrix* data = gsl_matrix_alloc(ndata,3);
	gsl_vector* c = gsl_vector_alloc(nfuncs);
	gsl_matrix* covariance = gsl_matrix_alloc(nfuncs,nfuncs);
	for(int i = 0; i<ndata; i++){
		gsl_matrix_set(data,i,0,x[i]);
		gsl_matrix_set(data,i,1,log(y[i]));
		gsl_matrix_set(data,i,2,(y[i]/20)/y[i]);
	}

	f functions[2] = {&f_0, &f_1};


	ls_fits(data, functions,nfuncs,c,covariance);

	double c_0 = gsl_vector_get(c,0);
	double c_1 = gsl_vector_get(c,1);

	FILE* fitvals = fopen("out.exerciseA.txt","w");
	
	fprintf(fitvals, "c_0 = %g\n c_1 lambda = %g\n, My T_1/2 = %g \n Actual T_1/2 = %g\n",c_0,c_1,log(2)/-c_1,3.63);
	fprintf(fitvals, "Difference from our calculation to real value: %g - %g = %g\n", log(2)/-c_1,3.63,log(2)/(-c_1)-3.63);
	fprintf(fitvals, "This is not within the estimated uncertainty, see covariance matrix.\n");
	fprintf(fitvals, "Note: covariance matrix is in out.txt\n");

	print_matrix(nfuncs, covariance, "Covariance matrix:");


	FILE* fdata = fopen("out.dataplot.txt","w");

	for(int i = 0; i<data->size1; i++){
		double x_i = gsl_matrix_get(data,i,0);
		double y_i = gsl_matrix_get(data,i,1);
		double dy_i = gsl_matrix_get(data,i,2);
		fprintf(fdata, "%g %g %g\n",x_i,y_i,dy_i);

	}
	FILE* fit = fopen("out.fitplot.txt","w");

	int rFP = 200;
	for(int i = 0; i<rFP; i++){
		double x_i = ((double) i)/rFP*x[ndata-1];
		double f_i = f_0(x_i)*c_0+f_1(x_i)*c_1;
		fprintf(fit,"%g %g\n",x_i,f_i);
	}
	return 0;
}
