#ifndef HAVE_UTILITIES_H
#define HAVE_UTILITIES_H

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include "backsub.h"

double funs(int i, double x);
void GS_decomp(gsl_matrix* A, gsl_matrix* R);
void GS_solve(gsl_matrix* Q, gsl_matrix* R, gsl_vector* b, gsl_vector* x);
void GS_inv(gsl_matrix* Q, gsl_matrix* R, gsl_matrix* B);
double randomnum( unsigned int *seed );
void vector_print(char* string, gsl_vector* vector);
void set_data(gsl_matrix* tmat, gsl_vector* tRvec, unsigned int *seed);
void print_matrix(int NoR, gsl_matrix* mTP, char* string );

#endif
