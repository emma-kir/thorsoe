#ifndef HAVE_LINEARSPLINE_H
#define HAVE_LINEARFPLINE_H

double linear_spline(int NoP, double *pts, double *funcVals, double Ept);
double linear_spline_integration(int NoP, double *pts, double *funcVals, double Ept);

#endif
