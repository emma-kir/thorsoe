#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_interp.h>
#include "linearSpline.h"
#include "utilities.h"

double linear_spline(int NoP, double* pts, double* funcVals, double Ept){
	int interval = binary_search(NoP, pts, Ept);
	double fdif = funcVals[interval+1]-funcVals[interval];
	double pdif = pts[interval+1]-pts[interval];
	double slope = fdif/pdif;

	double interpolationval = funcVals[interval]+slope*(Ept - pts[interval]);
	return interpolationval;
}

double linear_spline_integration(int NoP, double* pts, double* funcVals, double Ept){
	int interval = binary_search(NoP, pts, Ept);

	double integral = 0;
	double fdif;
	double pdif;
	double slope;

	for(int i = 0; i <= interval; i++){
		fdif = funcVals[i+1] - funcVals[i];
		pdif = pts[i+1] - pts[i];
		slope = fdif/pdif;

		if(i >= interval){
			pdif = Ept - pts[i];
		}
		integral += funcVals[i]*pdif + slope*pdif*pdif/2;
	}
	return integral;
}
