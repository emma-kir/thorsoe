#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_interp.h>
#include "utilities.h"
#include "linearSpline.h"
#include "quadraticSpline.h"
#include "integrateFunction.h"

double gsl_cos (double x, void * params) {
  /* Redefinition of signature of cos() from math.h, in order
     to ensure safe-cast to ‘double (*)(double,  void *)’ used
     with gsl routines, from an incompatible pointer type
     '__attribute__((const)) double (*)(double)’                */
  return cos(x);
}

void defIntegral( int numOfPts,
                  double* xData,
                  double* yData,
                  double lowerLimit,
                  double upperLimit,
                  double absError,
                  double relError,
                  size_t iterationLimit ){

    double integralVal = linear_spline_integration( numOfPts, xData, yData, upperLimit );

    gsl_function gslFuncCos;
    gslFuncCos.function    =  &gsl_cos;
    gslFuncCos.params      =  NULL;
    double integralValComp = integrateFunction( lowerLimit, upperLimit, &gslFuncCos, absError, relError, iterationLimit );

    printf("The integral of the interpolant            from x[0] = %g to z = %g is         = %g\n",       lowerLimit, upperLimit, integralVal     );
    printf("The integral of cos(x) (from math.h)       from        %g to     %g using GSL  = %g\n",       lowerLimit, upperLimit, integralValComp );
    printf("The integral of cos(x) (from WolframAlpha) from        %g to     %g            = -0.99999\n", lowerLimit, upperLimit                  );
}


int main(int argc, char* argv[]){
	if(argc < 2){
		fprintf(stderr, "Error, no arguments were passed.\n");
		exit(-1);
	}

	int NoP = 20;
	int NoS = 500;

	char* inputFilename = argv[1];
	FILE* outFileStream_lin = fopen(argv[2], "w");

	double *xdat = malloc(NoP * sizeof(double));
	double *ydat = malloc(NoP * sizeof(double));

		input_to_array(xdat, ydat, inputFilename);

	double lowerLimit = xdat[0];
	double upperLimit = 11;
	double absError = 1e-6;
	double relError = 1e-6;
	size_t iterationLimit = 999;



	defIntegral(NoP, xdat, ydat, lowerLimit, upperLimit, absError, relError, iterationLimit);

	gsl_interp* gslInterp_lin = gsl_interp_alloc(gsl_interp_linear, NoP);
	gsl_interp_init(gslInterp_lin, xdat, ydat, NoP);


	double resolution = fabs(xdat[NoP-1] - xdat[0])/NoS;
	for(double Ept = xdat[0]; Ept < xdat[NoP]; Ept += resolution){
		double myinterpo = linear_spline(NoP, xdat, ydat, Ept);
		double gslinterpo = gsl_interp_eval(gslInterp_lin, xdat, ydat, Ept, NULL);
		double myintegr = linear_spline_integration(NoP, xdat, ydat, Ept);
		double gslintegr = gsl_interp_eval_integ(gslInterp_lin, xdat, ydat, xdat[0], Ept, NULL);

		fprintf(outFileStream_lin, "%g\t%g\t%g\t%g\t%g\n", Ept, myinterpo, gslinterpo, myintegr, gslintegr);
	}

	fclose(outFileStream_lin);
	gsl_interp_free(gslInterp_lin);

	return 0;
}
