#ifndef HAVE_UTILITIES_H
#define HAVE_UTILITIES_H

#include <stdio.h>
#include <stdlib.h>
#include <gsl/gsl_integration.h>

int binary_search(int NoP, double* pts, double Ept);
double integrate_GSL_function(double lower, double upper, const gsl_function* gslFunction, double tolAbs, double tolRel, size_t iterationLimit);
void input_to_array(double* xdata, double* ydata, char* inputFileName);

#endif

