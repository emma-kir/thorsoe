#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include "utilities.h"
#include "quadraticSpline.h"

quadSpline *initialize_quadratic_spline(int NoP, double *pts, double* FVoP){
	quadSpline *spline = (quadSpline *) malloc(sizeof(quadSpline));

	int NoE = NoP - 1;
	
	spline->NoP = NoP;
	spline->pts = (double *)(NoE*sizeof(double));
	spline->FVoP = (double *)(NoE * sizeof(double));
	spline->firstcoef = (double *) malloc(NoE * sizeof(double));
	spline->seccoef = (double*) malloc(NoE * sizeof(double));

	for (int i=0; i < NoP; i++){
		spline->pts[i] = pts[i];
		spline->FVoP[i] = FVoP[i];
	}

	double ptsDif[NoP - 1];
	double slope[NoP - 1];

	for (int i = 0; i < NoP; i++){
		ptsDif[i] = pts[i+1] - pts[i];
		slope[i] = (FVoP[i+1] - FVoP[i])/ptsDif[i];
	}

	spline->seccoef[0] = 0;
	for (int i=0; i<NoP -2; i++){
		spline->seccoef[i+1] = (slope[i+1] - slope[i] - spline->seccoef[i] * ptsDif[i])/ptsDif[i+1];
	}

	spline->seccoef[NoP-2] /= 2;
	for (int i = NoP -3; i >= 0; i--){
		spline->seccoef[i] = (slope[i+1] - slope[i] - spline->seccoef[i+1]*ptsDif[i+1])/ptsDif[i];
	}

	for (int i=0; i<NoP -1; i++){
		spline->firstcoef[i] = slope[i] - spline->seccoef[i]*ptsDif[i];
	}
	return spline;
}

double evaluate_quadratic_spline(quadSpline *spline, double EptsInt){
	int Interval = binary_search(spline->NoP, spline->pts, EptsInt);
	double ptsDif = EptsInt - spline->pts[Interval];
	double IntpolVal = spline ->FVoP[Interval] + ptsDif * (spline->firstcoef[Interval] + ptsDif * spline->seccoef[Interval]);
	return IntpolVal;
}

double evaluate_quadratic_spline_derivative(quadSpline *spline, double EptsInt){
	int Interval = binary_search(spline->NoP, spline->pts, EptsInt);
	double ptsDif = EptsInt - spline->pts[Interval];
	double intDerVal = spline->firstcoef[Interval] + 2 * ptsDif * spline->seccoef[Interval];
	return intDerVal;
}

double evaluate_quadratic_spline_integral(quadSpline *spline, double EptsInt){
	int Interval = binary_search(spline->NoP, spline->pts, EptsInt);
	double integralval = 0;
	double ptsDif;

	for (int i = 0; i<= Interval; i++){
		if(i < Interval){ptsDif = spline->pts[i+1] - spline->pts[i];}
		else {ptsDif = EptsInt - spline->pts[i];}


		integralval += spline->FVoP[i] * ptsDif + spline->firstcoef[i] * ptsDif * ptsDif/2 + spline->seccoef[i] * ptsDif * ptsDif * ptsDif / 3;
	}
	return integralval;
}
