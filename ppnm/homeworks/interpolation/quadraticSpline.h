#ifndef HAVE_QUADRATICSPLINE_H
#define HAVE_QUADRATICSPLINE_H

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef struct{
	int NoP;
	double* pts;
	double* FVoP;
	double* firstcoef;
	double* seccoef;
} quadSpline;

quadSpline *initialize_quadratic_spline(int NoP, double* pts, double* FVoP);
double evaluate_quadratic_spline(quadSpline *spline, double EptInt);
double evaluate_quadratic_spline_derivative(quadSpline *spline, double EptInt);
double evaluate_quadratic_spline_integral(quadSpline *spline, double EptInt);

#endif
