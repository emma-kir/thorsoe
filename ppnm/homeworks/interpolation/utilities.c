#include <stdio.h>
#include <stdlib.h>
#include <gsl/gsl_integration.h>
#include <math.h>
#include "utilities.h"

int binary_search(int NoP, double* pts, double Ept){
	int left = 0;
	int right = NoP - 1;

	while (right - left > 1){
		int middle = (right + left)/2;
		
		if (Ept > pts[middle]){
			left = middle;
		}
		else {
			right = middle;
		}
	}
	return left;
}

double integrate_GSL_function(double lower, double upper, const gsl_function* gslFunction, double tolAbs, double tolRel, size_t iterationlimit){
	gsl_integration_workspace* workspace = gsl_integration_workspace_alloc(iterationlimit);

	double result;
	double Abserr;

	gsl_integration_qags(gslFunction, lower, upper, tolAbs, tolRel, iterationlimit, workspace, &result, &tolAbs);
	workspace = NULL;

	return result;
}

void input_to_array(double* xdata, double* ydata, char* inputFilename){
	int NoRV = 2;
	int input = NoRV;
	double argx;
	double argy;

	FILE* mIFS = fopen(inputFilename, "r");

	int id = 0;

	while (input != EOF){
		if( input == NoRV){
			input = fscanf(mIFS, "%lg\t%lg", &argx, &argy);

			xdata[id] = argx;
			ydata[id] = argy;
			id++;
		}
		else {
			fprintf(stderr, "Failed to read input.\n");
			exit(-1);
		}
	}
	fclose(mIFS);
}
