#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <gsl/gsl_vector.h>

#include "newtonmeth.h"
#include "gramschmidt.h"

void testfunc(gsl_vector* vals, gsl_vector* funcvals){
	double x = gsl_vector_get(vals,0);
	double y = gsl_vector_get(vals,1);

	double a = 6;
	double b = 13;

	gsl_vector_set(funcvals, 0, 2*x*(x-a));
	gsl_vector_set(funcvals, 1, 2*y*(y-b));
}

void rosenbrock(gsl_vector* vals, gsl_vector* funcvals){
	double x = gsl_vector_get(vals,0);
	double y = gsl_vector_get(vals,1);

	gsl_vector_set(funcvals, 0, (-1)*2*(1-x) + (-2*x)*2*100*(y-x*x));
	gsl_vector_set(funcvals, 1, 2*100*(y-x*x));
}

int main(int argc, char* argv[]){

	int dim = 2;
	double tol = 1e-5;
	gsl_vector* min = gsl_vector_alloc(dim);
	gsl_vector_set(min, 0, 4);
	gsl_vector_set(min, 1, 10);

	printf("Testing for function f(x) = 1 + (x-a)² + (y-b)²: \n Using a = 6 and b = 13\n");
	printf("Initial guess: (x,y) = (%g,%g)\n", gsl_vector_get(min, 0), gsl_vector_get(min,1));
	printf("Actual minimum: (x,y) = (6,13)\n");
	newton(testfunc, min, tol);
	printf("Minimum found: (x,y) = (%g,%g)\n", gsl_vector_get(min, 0), gsl_vector_get(min,1));

	gsl_vector_set(min, 0, 0.5);
	gsl_vector_set(min, 1, 0.5);

	printf("Testing for Rosenbrock's valley function:");
	printf("Initial guess: (x,y) = (%g,%g)\n", gsl_vector_get(min, 0), gsl_vector_get(min,1));
	printf("Actual minimum: (x,y) = (6,13)\n");
	newton(rosenbrock, min, tol);
	printf("Minimum found: (x,y) = (%g,%g)\n", gsl_vector_get(min, 0), gsl_vector_get(min,1));


}
