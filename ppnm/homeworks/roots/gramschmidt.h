#ifndef HAVE_GRAMSCHMIDT_DECOMP_H
#define HAVE_GRAMSCHMIDT_DECOMP_H

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>

void GS_decomp(gsl_matrix* A, gsl_matrix* R);
void GS_solve(gsl_matrix* Q, gsl_matrix* R, gsl_vector* b, gsl_vector* xvec);

#endif
