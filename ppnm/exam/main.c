#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>

#include "bilinear.h"
#include "gramschmidt.h"



double testfunc (double x, double y){ //Function to make the matrix
        double a = 6;
        double b = 13;

        double funcval = 1 + (x-a)*(x-a) + (y-b)*(y-b);

        return funcval;
}

int main(){
	double mrand = RAND_MAX;
	int nx = 10;
	int ny = 9;

	gsl_vector* x = gsl_vector_alloc(nx);
	gsl_vector* y = gsl_vector_alloc(ny);
	gsl_matrix* F = gsl_matrix_alloc(nx,ny);

	for(int i = 0; i < x->size; i++){ //Making the vectors and matrix to enter into the function
		double x_i = rand()/mrand;
		gsl_vector_set(x,i,x_i);
	}
	for(int i = 0; i < y->size; i++){
		double y_i = rand()/mrand;
		gsl_vector_set(y,i,y_i);
	}

	for(int i = 0; i < x->size; i++){
		double x_i = gsl_vector_get(x,i);
		for(int j = 0; j < y->size; j++){
			double y_j = gsl_vector_get(y,j);

			double F_ij = testfunc(x_i,y_j);

			gsl_matrix_set(F,i,j,F_ij);
		}
	}

	vector_print("x =", x);
	vector_print("y =", y);
	print_matrix(nx, F, "F =");

	int px = 5;
	int py = 7;

	double result = bilinear(x,y,F,px,py); //Calling the function, note the indices are counted from 0.

	printf("\n\nValue calculated: %g\n", result);
	printf("Actual value: %g\n", gsl_matrix_get(F,px,py));
	printf("Difference: %g\n", fabs(result - gsl_matrix_get(F,px,py)));

return 0;
}
