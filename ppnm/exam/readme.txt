Name: Emma Kirstine Holden Thorsøe
Student ID: 201704903

Exam project: 3 - Bi-linear interpolation on a rectilinear grid

Problem: 
Build an interpolating routine which takes as the input the vectors {x} and {y} and the matrix {F} and returns the 
bi-linear interpolated value of the function at a given 2D-point p={p_x,p_y}.

Description:
In order to do bi-linear interpolation I need information from four surrounding points. I do this by subtracting one from px 
and py, to make the indices x1 = px-1 and y1 = py-1, and also adding one to px and py to make x2 = px+1 and y2 = py+1. 
If an index is on the 'border' of the matrix, the code takes the new index from the other side of the matrix.
I then combine these indeces to access the surrounding points q11 = F[x1,y1], q12 = F[x1,y2], q21 = F[x2,y1], and q22 = F[x2,y2].
I also need the values of x1, x2, y1, and y2 from their respective vectors. The notation for these values being x_x1 = x_1, 
x_x2 = x_2, y_y1 = y_1, and y_y2 = y_2.

These are then entered into the following linear system Az=b:

	| 1	x_1	y_1	x_1*y_1 | | a_0 |   | q11 |
	|				| |     |   |     |
	| 1	x_1	y_2	x_1*y_2 | | a_1 |   | q12 |
	|				| |     | = |     |
	| 1	x_2	y_1	x_2*y_1 | | a_2 |   | q21 |
	|				| |     |   |     |
	| 1	x_2	y_2	x_2*y_2 | | a_3 |   | q22 |

Which is solved using the Gram-Schmidt orthogonalization and backsubstitution to solve for the vector z, which contains the 
coefficients that I need, since I can use these to find the value I am looking for:

	F[px,py] = a_0 + a_1*x[px] + a_2*y[py] + a_3*x[px]*y[py]

 
