#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_vector.h>
#include <assert.h>

#include "gramschmidt.h"

void backsub(gsl_matrix* A, gsl_vector* Rvec){
        int NoR = (Rvec -> size);

        for (int row = NoR - 1; row >= 0; row--){
                double Rval = gsl_vector_get(Rvec, row);

                for(int vari = row + 1; vari < NoR; vari++){
                        Rval -= gsl_matrix_get(A, row, vari)*gsl_vector_get(Rvec,vari);
                }
        gsl_vector_set(Rvec, row, Rval/gsl_matrix_get(A, row, row));
        }
}


void GS_decomp(gsl_matrix* A, gsl_matrix* R){
        int NoR =  A -> size1;
        int NoC =  A -> size2;
        assert(NoR >= NoC);

        for(int colu = 0; colu < NoC; colu++){
                gsl_vector* col = gsl_vector_alloc(NoR);
                *col = gsl_matrix_column(A, colu).vector;
                double colnorm = gsl_blas_dnrm2(col);
                gsl_matrix_set(R, colu, colu, colnorm);

                gsl_vector* oMC = gsl_vector_alloc(NoR);

                gsl_vector_memcpy(oMC, col);
                gsl_vector_scale(oMC, 1./colnorm);
                gsl_matrix_set_col(A, colu, oMC);

                for(int ncolu = colu+1; ncolu < NoC; ncolu++){
                        gsl_vector* ncol = gsl_vector_alloc(NoR);
                        *ncol = (gsl_matrix_column(A, ncolu)).vector;

                        double tME;
                        gsl_blas_ddot(oMC, ncol, &tME);
                        gsl_matrix_set(R,colu,ncolu,tME);
                        gsl_vector* oCS = gsl_vector_alloc(NoR);
                        gsl_vector_memcpy(oCS,oMC);
                        gsl_vector_scale(oCS, tME);
                        gsl_vector_sub(ncol,oCS);

                        gsl_matrix_set_col(A,ncolu,ncol);

                        gsl_vector_free(ncol);
                        gsl_vector_free(oCS);
                }
                gsl_vector_free(col);
                gsl_vector_free(oMC);

        }
}

void GS_solve(gsl_matrix* Q, gsl_matrix* R, gsl_vector* b, gsl_vector* xvec){
        gsl_blas_dgemv(CblasTrans, 1, Q, b, 0, xvec);
        backsub(R, xvec);
}
