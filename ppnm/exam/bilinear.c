#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>

#include "bilinear.h"
#include "gramschmidt.h"

void print_matrix(int NoR, gsl_matrix* mTP, char* string){
        printf("\n%s\n", string);
        for(int row = 0; row < NoR; row++){
                gsl_vector_view mTP_row = gsl_matrix_row(mTP, row);
                gsl_vector* vec = &mTP_row.vector;

                for(int iter = 0; iter < vec -> size; iter++){
                        if(gsl_vector_get(vec,iter) > 1e-10){
                                printf("%10g\t",gsl_vector_get(vec,iter));
                        }
                        else {printf("%10g\t", 0.0);}
                }
        printf("\n");
        }
}

void vector_print(char* string, gsl_vector* vector){
        printf("%s\n", string);
        for (int iter = 0; iter < vector -> size; iter++){
                printf("%15g", gsl_vector_get(vector, iter));
        }
        printf("\n");
}


double bilinear(gsl_vector* x, gsl_vector* y, gsl_matrix* F, double px, double py){
	gsl_matrix* A = gsl_matrix_calloc(4,4);
	gsl_matrix* R = gsl_matrix_alloc(4,4);
	gsl_vector* b = gsl_vector_alloc(4);
	gsl_vector* res = gsl_vector_alloc(4);

	double x_px = gsl_vector_get(x,px);
	double y_py = gsl_vector_get(y,py);

	int x1 = px - 1; //finding the indices that will match surrounding points
	int x2 = px + 1;
	int y1 = py - 1;
	int y2 = py + 1;

	if(px == 0){x1 = x->size - 1;} //Taking into account if px and/or py is a border value
	else if(px == x->size - 1){x2 = 0;}

	if(py == 0){y1 = y->size - 1;}
	else if(py == y->size - 1){y2 = 0;}

	double q11 = gsl_matrix_get(F, x1, y1); //finding entries for result vector in linear system
	double q12 = gsl_matrix_get(F, x1, y2);
	double q21 = gsl_matrix_get(F, x2, y1);
	double q22 = gsl_matrix_get(F, x2, y2);

	gsl_vector_set(b,0,q11);
	gsl_vector_set(b,1,q12);
	gsl_vector_set(b,2,q21);
	gsl_vector_set(b,3,q22);

	double x_1 = gsl_vector_get(x,x1); //finding entries for the A matrix in the linear system
	double x_2 = gsl_vector_get(x,x2);
	double y_1 = gsl_vector_get(y,y1);
	double y_2 = gsl_vector_get(y,y2);

	gsl_matrix_set(A,0,0,1);
	gsl_matrix_set(A,1,0,1);
	gsl_matrix_set(A,2,0,1);
	gsl_matrix_set(A,3,0,1);

	gsl_matrix_set(A,0,1,x_1);
	gsl_matrix_set(A,1,1,x_1);
	gsl_matrix_set(A,2,1,x_2);
	gsl_matrix_set(A,3,1,x_2);

	gsl_matrix_set(A,0,2,y_1);
	gsl_matrix_set(A,1,2,y_2);
	gsl_matrix_set(A,2,2,y_1);
	gsl_matrix_set(A,3,2,y_2);

	gsl_matrix_set(A,0,3,x_1*y_1);
	gsl_matrix_set(A,1,3,x_1*y_2);
	gsl_matrix_set(A,2,3,x_2*y_1);
	gsl_matrix_set(A,3,3,x_2*y_2);

	print_matrix(4,A,"A =");

	GS_decomp(A,R); //Using the code from the linear systems homework to solve the system
	GS_solve(A,R,b,res);

	vector_print("\nThe coefficients are:", res);

	double a_0 = gsl_vector_get(res,0);
	double a_1 = gsl_vector_get(res,1);
	double a_2 = gsl_vector_get(res,2);
	double a_3 = gsl_vector_get(res,3);

	double result = a_0 + a_1*x_px + a_2*y_py + a_3*x_px*y_py; //using the results to find the value wanted
	return result;
}

