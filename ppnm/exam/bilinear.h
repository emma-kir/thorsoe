#ifndef HAVE_MINIMIZATION_H
#define HAVE_MINIMIZATION_H

void print_matrix(int NoR, gsl_matrix* mTP, char* string);
void vector_print(char* string, gsl_vector* vector);
double bilinear(gsl_vector* x, gsl_vector* y, gsl_matrix* F, double px, double py);

#endif
