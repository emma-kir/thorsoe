#include <stdio.h>
#include <math.h>
#include <gsl/gsl_integration.h>

double myfunc(double x, void* params){
	double value = log(x)/sqrt(x);
	return value;
}

double myint(){
	gsl_function F;
	F.function = &myfunc;
	int limit = 999;
	gsl_integration_workspace* w = gsl_integration_workspace_alloc(limit);
	double a = 0, b = 1, abserr = 1e-6, relerr = 1e-6, result, error;
	gsl_integration_qags(&F, a, b, abserr, relerr, limit, w, &result, &error);
	gsl_integration_workspace_free(w);
	return result;
}

int main(){
	FILE* out = fopen("out.txt","w");
	double resa = myint();
	fprintf(out, "int(log(x)/sqrt(x), x=0..1) = %g\n",resa);

return 0;
}
