#include <stdio.h>
#include <assert.h>
#include "vector.h"

vector* vector_alloc(int n){
  vector* v = malloc(sizeof(vector));
  (*v).size = n;
  (*v).data = malloc(n*sizeof(double));
  if( v==NULL ) fprintf(stderr,"error in vector_alloc\n");
  return v;
}

void vector_free(vector* v){ free(v->data); free(v);} /* v->data is identical to (*v).data */
void vector_set(vector* v, int i, double value){
	assert( 0 <= i && i < (*v).size );
	(*v).data[i]=value;
}
double vector_get(vector* v, int i){
	assert( 0 <= i && i < (*v).size );
	return (*v).data[i];
}

double vector_dot_product(vector* u, vector* v){
	double res = 0;
	for(int i = 0; i < (*v).size; i++){
		double u_i = vector_get(u,i);
		double v_i = vector_get(v,i);

		res += u_i*v_i;
	}
	return res;
}
