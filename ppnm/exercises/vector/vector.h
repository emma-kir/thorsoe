#ifndef HAVE_VECTOR_H /* for multiple includes */
#define HAVE_VECTOR_H

typedef struct {int size; double* data;} vector;

vector* vector_alloc       (int n);     /* allocates memory for size-n vector */
void    vector_free        (vector* v);                      /* frees memory */
void    vector_set         (vector* v, int i, double value); /* v_i ← value */;
double  vector_get         (vector* v, int i);              /* returns v_i */
double  vector_dot_product (vector* u, vector* v);   /* returns dot-product */

#endif
