#include "vector.h"
#include "stdio.h"
#include "stdlib.h"
#define RND (double)rand()/RAND_MAX

int main()
{
	int n = 5;

	printf("\nmain: testing vector_alloc ...\n");
	vector *v = vector_alloc(n);
	if (v == NULL) printf("test failed\n");
	else printf("test passed\n");

	printf("\nmain: testing vector_set and vector_get ...\n");
	double value = RND;
	int i = n / 2;
	vector_set(v, i, value);
	double vi = vector_get(v, i);
	if (double_equal(vi, value)) printf("test passed\n");
	else printf("test failed\n");

	printf("\nmain: testing vector_add ...\n");
	vector *a = vector_alloc(n);
	vector *b = vector_alloc(n);
	vector *c = vector_alloc(n);
	for (int i = 0; i < n; i++) {
		double x = RND, y = RND;
		vector_set(a, i, x);
		vector_set(b, i, y);
		vector_set(c, i, x + y);
	}
	vector_add(a, b);
	vector_print("a+b should   = ", c);
	vector_print("a+b actually = ", a);

	if (vector_equal(c, a)) printf("test passed\n");
	else printf("test failed\n");

	vector_free(v);
	vector_free(a);
	vector_free(b);
	vector_free(c);

	return 0;
}
