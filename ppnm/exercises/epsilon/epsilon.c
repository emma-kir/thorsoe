#include <limits.h>
#include <float.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>


int main(){
	printf("\n1 i)\n");

	printf("\nINT_MAX = %d\n",INT_MAX);
	
	int i=1; 
	while(i+1>i) 
		{i++;}
	printf("my max integer in while = %i\n",i);

	int out=1; 
	for( int n = 1; n<n+1;n++) {out = n;}

	printf("my max integer in for loop = %i\n",out);

	int l = 1;
	do{l++;} while(l<l+1);

	printf("My max interger in do... while loop = %i\n",l);


	printf("\n1 ii)\n");

	printf("\nINT_MIN = %d\n",INT_MIN);

        int m=1;
        while(m-1<m)
                {m--;}
        printf("my min integer in while = %i\n",m);

        int p=1;
        for( int n = 1; n-1<n;n--) {p = n;}

        printf("my min integer in for loop = %i\n",p);

        int o = 1;
        do{o--;} while(o-1<o);

        printf("My min integer in do... while loop = %i\n",o);

	printf("\n1 iii)\n");

	printf("\nFLT_EPSILON = %g\n", FLT_EPSILON);
	printf("DBL_EPSILON = %g\n",DBL_EPSILON);
	printf("LDBL_EPSILON = %Lg\n",LDBL_EPSILON);

	printf("\nFloats\n");

	float xf=1;
        while(1+xf!=1){
        xf/=2;
        }
        xf*=2;
        printf("while = %g\n",xf);

        float yf;
        for(yf=1; 1+yf!=1;yf/=2){}
        yf*=2;
        printf("for = %g\n",yf);

        float zf = 1;
        do{zf/=2;} while(1+zf!=1);
        zf*=2;
        printf("do.. while = %g\n",zf);



	printf("\nDoubles");
	double x=1;
	while(1+x!=1){
	x/=2;
	}
	x*=2;
	printf("\nwhile = %g\n",x);

	double y;
	for(y=1; 1+y!=1;y/=2){}
	y*=2;
	printf("for = %g\n",y);

	double z = 1;
	do{z/=2;} while(1+z!=1);
	z*=2;
	printf("do.. while = %g\n",z);

	printf("\nLong doubles");
        long double xl=1;
        while(1+xl!=1){
        xl/=2;
        }
        xl*=2;
        printf("\nwhile = %Lg\n",xl);

        long double yl;
        for(yl=1; 1+yl!=1;yl/=2){}
        yl*=2;
        printf("for = %Lg\n",yl);

        long double zl = 1;
        do{zl/=2;} while(1+zl!=1);
        zl*=2;
        printf("do.. while = %Lg\n",zl);


	printf("\n2");
	printf("Note: max = 1000, since computation time was very long otherwise\n");

	int max = 1000;
	float please = 1;
	float var = 0;
	while(please-1!=max){
	float addi = 1/please;
	var=var + addi;
	please++;
	//printf("\nsum up = %f, please = %f\n",var,addi);
	}

	printf("\nSum_up = %f\n",var);
	

	float varn = 0;
	float pleasn = max;
	
	while(pleasn != 0){
	varn = varn+1/pleasn;
	pleasn = pleasn - 1;
	}

	printf("Sum_down = %f\n",varn);


        double pleased = 1;
        double vard = 0;
        while(pleased-1!=max){
        double addid = 1/pleased;
        vard=vard + addid;
        pleased++;
        //printf("\nsum up = %f, please = %f\n",var,addi);
        }

        printf("\nSum_up_double = %f\n",vard);


        double varnd = 0;
        double pleasnd = max;

        while(pleasnd != 0){
        varnd = varnd+1/pleasnd;
        pleasnd = pleasnd - 1;
        }

        printf("Sum_down_double = %f\n",varnd);

return 0;
}
