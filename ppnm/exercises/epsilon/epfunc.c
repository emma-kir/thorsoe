#include <limits.h>
#include <float.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>


int epfunc(double a, double b, double tau, double epsilon){
	if(fabs(a-b)<tau || fabs(a-b)/(fabs(a)+fabs(b))<epsilon/2){return 1;}
	else{return 0;}
}

int main() {
	printf("\n3)\n");
	double a = 5;
	double b = 6;
	double tau = 1;
	double epsilon = 2;
	printf("\ntesting with a = %f, b = %f, tau = %f, epsilon = %f, function return: %i\n",a,b,tau,epsilon, epfunc(a,b,tau,epsilon));
return 0;
}
