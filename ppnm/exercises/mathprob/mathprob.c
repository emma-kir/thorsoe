#include <math.h>
#include <complex.h>
#include <stdio.h>

int main(void){
	printf("\nMath exercise\n");
	double g = gamma(5);
	double b = j1(0.5);
	complex z = csqrt(-2);
	complex a = cexp(I*M_PI);
	complex c = cexp(I);
	complex d = cpow(I,M_E);
	complex e = cpow(I,I);

	printf("\nExercise 1:\n");
	printf("g = %f\n",g);
	printf("b = %f\n",b);
	printf("z = %f + I%f\n",creal(z),cimag(z));
	printf("a = %f + I%f\n",creal(a),cimag(a));
	printf("c = %f + I%f\n",creal(c),cimag(c));
	printf("d = %f + I%f\n",creal(d),cimag(d));
	printf("e = %f + I%f\n\n",creal(e),cimag(e));




	float x_float = 1.f/9;
	double x_double = 1./9;
	long double x_long_double = 1.L/9;

	printf("Exercise 2:\n");
	printf("x_float = %.25g\n",x_float);
	printf("x_double = %.25lg\n",x_double);
	printf("x_long_double = %.25Lg\n",x_long_double);
return 0;
}
